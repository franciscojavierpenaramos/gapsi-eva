package com.javier.gapsi.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.icu.text.NumberFormat;
import android.icu.util.Currency;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.javier.gapsi.R;
import com.javier.gapsi.api.models.Record;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductHolder>{

    private List<Record> products;
    private NumberFormat numberFormat;
    private Context context;

    public ProductAdapter(Context context, List<Record> products) {
        this.products = products;

        numberFormat = NumberFormat.getCurrencyInstance();
        numberFormat.setMaximumFractionDigits(0);
        numberFormat.setCurrency(Currency.getInstance(Locale.getDefault()));
    }

    @NonNull
    @Override
    public ProductHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_item, parent, false);
        return new ProductHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductHolder holder, int position) {
        Record product = products.get(position);
        holder.lbName.setText(product.getProductDisplayName());
        double listPrice = product.getListPrice();
        double promoPrice = product.getPromoPrice();
        holder.variantList.setAdapter(new VariantAdapter(product.getVariantsColor()));
        Picasso.get().load(product.getLargeImage()).into(holder.productImage);

        if(promoPrice < listPrice && promoPrice > 0){
            holder.lbPrice.setText(numberFormat.format(promoPrice));
            holder.lbOldPrice.setText(numberFormat.format(listPrice));
            holder.lbOldPrice.setVisibility(View.VISIBLE);
        }else {
            holder.lbPrice.setText(numberFormat.format(listPrice));
            holder.lbOldPrice.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public class ProductHolder extends RecyclerView.ViewHolder{

        private ImageView productImage;
        private TextView lbName;
        private TextView lbOldPrice;
        private TextView lbPrice;
        private RecyclerView variantList;

        public ProductHolder(@NonNull View itemView) {
            super(itemView);
            productImage = itemView.findViewById(R.id.productImage);
            lbName = itemView.findViewById(R.id.lbName);
            lbOldPrice = itemView.findViewById(R.id.lbOldPrice);
            lbPrice = itemView.findViewById(R.id.lbPrice);
            variantList = itemView.findViewById(R.id.variantList);
            lbOldPrice.setPaintFlags(lbOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            LinearLayoutManager layoutManager
                    = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            variantList.setLayoutManager(layoutManager);
        }
    }
}
