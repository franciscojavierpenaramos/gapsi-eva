package com.javier.gapsi.adapter;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.javier.gapsi.R;
import com.javier.gapsi.api.models.VariantColor;

import java.util.Collections;
import java.util.List;

public class VariantAdapter extends RecyclerView.Adapter<VariantAdapter.VariantHolder>{

    private List<VariantColor> variantColors;

    public VariantAdapter(List<VariantColor> variantColors) {
        if(variantColors == null){
            variantColors = Collections.emptyList();
        }
        this.variantColors = variantColors;
    }

    @NonNull
    @Override
    public VariantHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.variant_item, parent, false);
        return new VariantHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull VariantHolder holder, int position) {
        VariantColor variantColor = variantColors.get(position);
        Drawable background = holder.color.getBackground();
        if (background instanceof ShapeDrawable) {
            ((ShapeDrawable)background).getPaint().setColor(Color.parseColor(variantColor.getColorHex()));
        } else if (background instanceof GradientDrawable) {
            ((GradientDrawable)background).setColor(Color.parseColor(variantColor.getColorHex()));
        } else if (background instanceof ColorDrawable) {
            ((ColorDrawable)background).setColor(Color.parseColor(variantColor.getColorHex()));
        }
    }

    @Override
    public int getItemCount() {
        return variantColors.size();
    }

    public class VariantHolder extends RecyclerView.ViewHolder{

        private View color;

        public VariantHolder(@NonNull View itemView) {
            super(itemView);
            color = itemView.findViewById(R.id.color);
        }
    }
}
