package com.javier.gapsi.api.models;

import lombok.Data;

@Data
public class State {
    private String categoryId;
    private String currentSortOption;
    private String currentFilters;
    private int firstRecNum;
    private int lastRecNum;
    private int recsPerPage;
    private int totalNumRecs;
}
