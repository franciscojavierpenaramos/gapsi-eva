package com.javier.gapsi.api.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Data;

@Data
public class Record {
    private String productId;
    private String skuRepositoryId;
    private String productDisplayName;
    private String productType;
    private int productRatingCount;
    private double productAvgRating;
    private double listPrice;
    private double minimumListPrice;
    private double maximumListPrice;
    private double promoPrice;
    private double minimumPromoPrice;
    private double maximumPromoPrice;
    @SerializedName("isHybrid")
    private boolean hybrid;
    @SerializedName("isMarketPlace")
    private boolean marketPlace;
    @SerializedName("isImportationProduct")
    private boolean importationProduct;
    @SerializedName("smImage")
    private String smallImage;
    @SerializedName("lgImage")
    private String largeImage;
    @SerializedName("xlImage")
    private String xLargeImage;
    private String groupType;
    private List<VariantColor> variantsColor;
}
