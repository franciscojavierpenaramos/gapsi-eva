package com.javier.gapsi.api.models;

import lombok.Data;

@Data
public class SortOption {
    private String sortBy;
    private String label;
}
