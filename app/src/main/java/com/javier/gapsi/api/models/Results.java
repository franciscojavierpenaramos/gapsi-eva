package com.javier.gapsi.api.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Data;

@Data
public class Results {
    private String label;
    @SerializedName("plpState")
    private State state;
    private List<SortOption> sortOptions;
    private List<RefinementGroup> refinementGroups;
    private List<Record> records;

}