package com.javier.gapsi.api.models;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class Stock {
    private Status status;
    private String pageType;
    @SerializedName("plpResults")
    private Results results;
}
