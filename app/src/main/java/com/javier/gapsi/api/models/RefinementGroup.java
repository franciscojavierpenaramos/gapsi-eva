package com.javier.gapsi.api.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Data;

@Data
public class RefinementGroup {
    private String name;
    @SerializedName("refinement")
    private List<Refinement> refinements;
    private boolean multiSelect;
    private String dimensionName;
}
