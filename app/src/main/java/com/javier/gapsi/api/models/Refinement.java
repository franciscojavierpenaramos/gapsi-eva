package com.javier.gapsi.api.models;

import lombok.Data;

@Data
public class Refinement {
    private int count;
    private String label;
    private String refinementId;
    private boolean selected;
}
