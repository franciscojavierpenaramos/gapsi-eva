package com.javier.gapsi.api;

import com.javier.gapsi.Constants;
import com.javier.gapsi.api.models.Stock;

import io.reactivex.Observable;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface LiverpoolService {
    class API{
        private static LiverpoolService instance;

        public static LiverpoolService getInstance(){
            if(instance == null){
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(Constants.LIVERPOOL_API)
                        .addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .build();
                instance = retrofit.create(LiverpoolService.class);
            }

            return instance;
        }
    }

    @GET("plp")
    Observable<Stock> search(@Query("force-plp") boolean forcePLP,
                             @Query("search-string") String searchString,
                             @Query("page-number") int pageNumber,
                             @Query("number-of-items-per-page") int numElementsPerPage);
}
