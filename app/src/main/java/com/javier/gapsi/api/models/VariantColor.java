package com.javier.gapsi.api.models;

import lombok.Data;

@Data
public class VariantColor {
    private String colorName;
    private String colorHex;
    private String colorImageURL;
}
