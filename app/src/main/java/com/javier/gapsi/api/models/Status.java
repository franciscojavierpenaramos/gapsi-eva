package com.javier.gapsi.api.models;

import lombok.Data;

@Data
public class Status {
    private String status;
    private int statusCode;
}
