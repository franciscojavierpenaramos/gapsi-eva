package com.javier.gapsi;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CursorAdapter;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.javier.gapsi.adapter.ProductAdapter;
import com.javier.gapsi.api.LiverpoolService;
import com.javier.gapsi.api.models.Record;
import com.javier.gapsi.utils.Suggestions;

import java.util.Collections;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    private ContentLoadingProgressBar progress;
    private RecyclerView productList;
    private CompositeDisposable disposables;
    private TextView emptyMessage;
    private ProductAdapter productAdapter;
    private SearchView searchView;
    private final String match = "productName";

    private List<String> SUGGESTIONS = Collections.emptyList();

    private SimpleCursorAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        productList = findViewById(R.id.productList);
        progress = findViewById(R.id.progress);
        emptyMessage = findViewById(R.id.emptyMessage);
        searchView = findViewById(R.id.searchView);

        disposables = new CompositeDisposable();

        SUGGESTIONS = Suggestions.getInstance(this).get();

        final String[] from = new String[] { match };
        final int[] to = new int[] {android.R.id.text1};
        mAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_1,
                null,
                from,
                to,
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

        searchView.setSuggestionsAdapter(mAdapter);
        searchView.onActionViewExpanded();

        // Getting selected (clicked) item suggestion
        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionClick(int position) {
                Cursor cursor = (Cursor) mAdapter.getItem(position);
                String txt = cursor.getString(cursor.getColumnIndex(match));
                searchView.setQuery(txt, true);
                search(txt);
                return true;
            }

            @Override
            public boolean onSuggestionSelect(int position) {
                // Your code here
                search(SUGGESTIONS.get(position));
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                if(!s.trim().isEmpty()){
                    search(s);
                    return true;
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                populateAdapter(s);
                return false;
            }
        });

        progress.hide();
    }

    @Override
    protected void onDestroy() {
        if(disposables != null){
            disposables.dispose();
        }
        super.onDestroy();
    }

    /**
     * Search text.
     * @param textToSearch
     */
    private void search(@NonNull String textToSearch){
        if(textToSearch.trim().isEmpty()){
            Toast.makeText(this, R.string.search_empty_message, Toast.LENGTH_SHORT).show();
            return;
        }

        hideKeyboard();
        showProgress();
        searchView.clearFocus();
        // Save new word.
        Suggestions.getInstance(this).saveValue(textToSearch);
        // Reload
        SUGGESTIONS = Suggestions.getInstance(this).get();

        Disposable d = LiverpoolService.API.getInstance()
                .search(true, textToSearch, 1, 50)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(stock -> {
                    hideProgress();
                    // Fill list.
                    if (stock.getStatus().getStatus().equals("OK")) {
                        List<Record> records = stock.getResults().getRecords();

                        if (records.size() > 0) {
                            emptyMessage.setVisibility(View.GONE);
                            productList.setVisibility(View.VISIBLE);
                        } else {
                            emptyMessage.setVisibility(View.VISIBLE);
                            productList.setVisibility(View.GONE);
                        }

                        productAdapter = new ProductAdapter(MainActivity.this, records);
                        productList.setAdapter(productAdapter);
                    } else {
                        Toast.makeText(MainActivity.this, String.valueOf(stock.getStatus().getStatusCode()), Toast.LENGTH_SHORT).show();
                    }
                }, throwable -> {
                    hideProgress();
                    Toast.makeText(MainActivity.this, getResources().getString(R.string.generic_fail_message), Toast.LENGTH_SHORT).show();
                });

        if(disposables != null){
            disposables.add(d);
        }
    }

    /**
     * Hide keyboard.
     */
    public void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            //Find the currently focused view, so we can grab the correct window token from it.
            View view = getCurrentFocus();
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = new View(this);
            }

            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Show progrees.
     */
    private void showProgress() {
        progress.show();
    }

    // Hide progress.
    private void hideProgress() {
        progress.hide();
    }

    private void populateAdapter(String query) {
        final MatrixCursor c = new MatrixCursor(new String[]{ BaseColumns._ID, match });
        for (int i = 0; i < SUGGESTIONS.size(); i++) {
            if (SUGGESTIONS.get(i).toLowerCase().startsWith(query.toLowerCase()))
                c.addRow(new Object[] {i, SUGGESTIONS.get(i)});
        }
        mAdapter.changeCursor(c);
    }
}