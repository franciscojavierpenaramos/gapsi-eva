package com.javier.gapsi.utils;

import android.content.Context;

import com.iamhabib.easy_preference.EasyPreference;

import java.util.ArrayList;
import java.util.List;

import lombok.NonNull;

public class Suggestions {

    private static Context context;
    private static Suggestions instance;
    private final String PREF_KEY = "PREFS";

    public static Suggestions getInstance(Context mContext){
        if(instance == null){
            instance = new Suggestions();
        }

        context = mContext;

        return instance;
    }

    /**
     * @param value
     */
    public void saveValue(@NonNull String value){
        List<String> current = get();
        if(!current.contains(value.toLowerCase())){
            current.add(value.toLowerCase());
        }

        save(current);
    }

    private void save( List<String> list){
        EasyPreference.with(context).addObject(PREF_KEY, list).save();
    }

    public List<String> get(){
        List<String> list = EasyPreference.with(context).getObject(PREF_KEY, List.class);
        if(list == null){
            list = new ArrayList<>();
        }

        return list;
    }
}
